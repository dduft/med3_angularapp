import { Component, Input } from "@angular/core";
import { User } from "src/app/models/user.model";

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
  })
  export class UserComponent {
    @Input() user!: User
    
    zeigeErweiterte = true

    constructor() {
    }
  

  toggleExtended(){
    this.zeigeErweiterte = !this.zeigeErweiterte
  }
}