import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[] = []
  currentUser: User = {
    name: '',
    username: '',
    email: ''
  }
  addUser = false

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userService.getUsers().subscribe((users) => {
      this.users = users
    })
  }

  saveUser(event: Event) {
    // update
    if(this.currentUser.id) {
      this.userService.updateUser(this.currentUser).subscribe((updatedUser)=> {
        this.currentUser.id = updatedUser.id

        this.clearUser()
      })
    //add 
    } else {
      this.userService.addUser(this.currentUser).subscribe((addedUser)=> {
        this.users.unshift(this.currentUser)
        this.clearUser()
      })
    }
    this.addUser = false

    event.preventDefault()
  }

  editUser(user: User) {
    this.currentUser = user
    this.addUser = true
  }

  private clearUser() {
    this.currentUser = {
      name: '',
      username: '',
      email: ''
    }
  }
}
