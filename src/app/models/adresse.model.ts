export interface Adresse {
    strasse: string
    nr: number
    plz: number
    ort: string
}