import { LOCALE_ID, NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { UserComponent } from './components/user/user.component'
import { UsersComponent } from './components/users/users.component'
import { NavbarComponent } from './components/navbar/navbar.component'
import { registerLocaleData } from '@angular/common'
import localeDe from '@angular/common/locales/de'
import localeDeExtra from '@angular/common/locales/extra/de'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from  '@angular/common/http';
import { HomeComponent } from './components/home/home.component'

registerLocaleData(localeDe, 'de-DE', localeDeExtra)

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UsersComponent,
    NavbarComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [{
    provide: LOCALE_ID,
    useValue: 'de-DE'
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
