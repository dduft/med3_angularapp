import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userUrl = "https://jsonplaceholder.typicode.com/users"

  constructor(
    private http: HttpClient
  ) {  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl)
  }

  addUser(user: User): Observable<User> {
    return this.http.post<User>(this.userUrl, user)
  }

  updateUser(user: User): Observable<User> {
    return this.http.put<User>(`${this.userUrl}/${user.id}`, user)
  }

  deleteUser(user: User): Observable<User> {
    return this.http.delete<User>(`${this.userUrl}/${user.id}`)
  }
}
